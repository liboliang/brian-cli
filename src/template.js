const templates = {
    'react': {
        url: 'https://gitee.com/liboliang/cli-template.git',
        downloadUrl: 'https://gitee.com:liboliang/cli-template.git#master',
        description: 'react-template'
    },
};

module.exports = {
    templates
}
