const m = require('moment');

function sayHello(){
    const dt = m().format('YYYY-MM-DD hh:mm:ss');
    console.log(`hi, this is brian's cli, now is ${dt}`);
}

module.exports = {
    sayHello,
}