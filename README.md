- 目前处于开发阶段...
- The package is being developed...

# 安装 install
```
npm i brian.li-cli -g
```

# 使用
初始化项目
```
brian-cli init [projectName]
```

查看帮助
```
brian-cli -h
```

查看现有的模板
```
brian-cli list
```
