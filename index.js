#!/usr/bin/env node
const { program } = require('commander');
const inquirer = require('inquirer');
const { templates } = require('./src/template');
const download = require('download-git-repo');
const handlebars = require('handlebars');
const chalk = require('chalk');
const fs = require('fs');
const ora = require('ora');


program.version('1.1.0')

program.command('init [name]')
       .description('init a project')
       // .option('-t, --type <type>', 'type of the project to init')
       .action((name, opt) => {
              //  console.log(`We are building a project called ${name} with ${opt.type}`);
              inquirer.prompt([
                     {
                            name: 'name',
                            message: '项目名',
                            default: name,
                     },
                     {
                            name: 'description',
                            message: '项目描述'
                     },
                     {
                            name: 'author',
                            message: '项目作者',
                            default: 'moon'
                     },
                     {
                            name: 'type',
                            type: 'list',
                            message: '模板类型',
                            choices: ['react'],
                            default: 'react',
                     }
              ]).then((res) => {
                     const { name, author, description, type } = res;
                     const { downloadUrl } = templates[type];
                     const spinner = ora('正在下载模板, 请稍后...');
                     spinner.start();
                     download(downloadUrl, name, { clone: true }, (err) => {
                            if (err) {
                                   spinner.fail();
                                   console.log(chalk.red('拉取模板失败',err.message));
                            } else {
                                   spinner.succeed();
                                   console.log(chalk.green('模板下载成功!'));
                                   const packagePath = `${name}/package.json`;
                                   const spinner_t = ora('正在初始化模板, 请稍后...');
                                   if (fs.existsSync(packagePath)) {
                                          const packageContent = fs.readFileSync(packagePath, 'utf8');
                                          const packageResult = handlebars.compile(packageContent)(res);
                                          fs.writeFileSync(packagePath, packageResult);
                                          spinner_t.succeed();
                                          console.log(chalk.green('初始化模版成功!'));
                                          console.log(
                                                 chalk.greenBright('开启项目') + '\n' +
                                                 chalk.greenBright('cd ' + name) + '\n' +
                                                 chalk.greenBright('npm i') + '\n' +
                                                 chalk.greenBright('npm run start')
                                          )
                                   } else {
                                          spinner_t.fail();
                                          console.log(chalk.red('下载成功,但找不到package.json!'));
                                          return;
                                   }

                            }
                     })
              })

       })

program.command('list')
       .description('check all template')
       .action(() => {
              const list = ['react', 'other templates are being developed...'];
              list.forEach(item => console.log(item));
       })

program.parse(process.argv)

